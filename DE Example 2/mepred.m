function V = mepred(t0,x0,y0,h,ul)
    a=3; b=2; 
    alpha=1; beta=2;

    xn=x0;
    yn=y0;
    tn=t0;
    Ti(1)=t0;
    Xi(1)=x0;
    Yi(1)=y0;
    n=1;
    while (n*h<=ul) 
        tnp1=tn+h;        
        xnp1est=xn+h*xn*(a-alpha*yn);
        ynp1est=yn+h*yn*(-b+beta*xn);
        xnp1=xn+(h/2)*(xn*(a-alpha*yn)+xnp1est*(a-alpha*ynp1est));
        ynp1=yn+(h/2)*(yn*(-b+beta*xn)+ynp1est*(-b+beta*xnp1est));
        Ti(n+1)=tnp1;
        Xi(n+1)=xnp1;
        Yi(n+1)=ynp1;
        tn=tnp1;
        xn=xnp1;
        yn=ynp1;
        n=n+1;
    end;
    V = [Ti; Xi; Yi];
