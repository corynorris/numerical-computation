figure

[T V]=ode23('predde',[0,4],[2;2]);
subplot(4,1,1), plot(V(:,1), V(:,2));

[T V]=ode45('predde',[0,4],[2;2]);
subplot(4,1,2), plot(V(:,1), V(:,2));

V=fepred(0, 2, 2, 0.1, 3);
subplot(4, 1, 3), plot(V(2,:), V(3,:));

V=mepred(0, 2, 2, 0.1, 3);
subplot(4,1,4), plot(V(2,:), V(3,:));
