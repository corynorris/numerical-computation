function dv = predde(t,xy)
    a=3; b=2; 
    alpha=1; beta=2;

    dx = xy(1) * (a-alpha*xy(2));
    dy = xy(2) * (-b+beta*xy(1));
    
    dv = [dx; dy];
    