function TiYi= mepop(t0,y0,h,ul)
   yn=y0;
   tn=t0;
   Ti(1)=t0;
   Yi(1)=y0;
   n=1;
   while (n*h<=ul) 
       tnp1=tn+h;
       ynp1est=yn+h*yn*(2.5*tn-tn^2*sqrt(yn));
       ynp1=yn+h/2*( yn*(2.5*tn-tn^2*sqrt(yn)) + ...
                     ynp1est*(2.5*tnp1-tnp1^2*sqrt(ynp1est)) );
       Ti(n+1)=tnp1;
       Yi(n+1)=ynp1;
       tn=tnp1;    
       yn=ynp1;
       n=n+1;
   end;
   TiYi=[Ti; Yi];
   
   
   
   
