﻿using System.IO;
using System;

public class MMSummary
{
    int n;
    public string[,] summary;

    public MMSummary(int n)
    {
        this.n = n;
        summary = new string[n, n];
        for (int k = 0; k < n; k++)
        {
            for (int i = 0; i < n; i++)
            {
                summary[k, i] = "A" + k + i;
            }
        }
    }

    public MMSummary(string[,] values, int n)
    {
        summary = values;
    }

    private int longestInCol(string[,] summary, int col)
    {
        int longest = 0;

        for (int k = 0; k < n; k++)
        {
            if (String.IsNullOrEmpty(summary[k, col]))
            {
                summary[k, col] = "A" + k + col;
            }
            if (summary[k, col].Length > longest)
            {
                longest = summary[k, col].Length;
            }

        }
        return longest;
    }

    public void print()
    {

        for (int k = 0; k < n; k++)
        {
            if (k == 0)
            {
                Console.Write("[");
            }
            else
            {
                Console.Write(" ");
            }
            for (int i = 0; i < n; i++)
            {
                if (String.IsNullOrEmpty(summary[k, i]))
                {
                    summary[k, i] = "A" + k + i;
                    Console.Write(summary[k, i]);
                }
                else
                {
                    Console.Write(summary[k, i]);
                }
                if (i < n - 1)
                {
                    Console.Write(", ");

                    //write spaces to make it look pretty
                    int longest = longestInCol(summary, i);
                    int spaces = longest - summary[k, i].Length;
                    for (int x = 0; x < spaces; x++)
                    {
                        Console.Write(" ");
                    }
                }
            }
            if (k == n - 1)
            {
                Console.WriteLine("]");
            }
            else
            {
                Console.WriteLine("");
            }
        }

    }
}

public class NNMatrix
{
    public int n;
    public double[,] values;
    public NNMatrix factored;
    public MMSummary operations;
    public MMSummary equations;

    public NNMatrix(double[,] values, int n)
    {
        this.n = n;
        this.values = values;
        LUFactorize();
    }
    public NNMatrix(NNMatrix copy)
    {
        this.values = (double[,])copy.values.Clone();
        this.n = copy.n;
    }
    private void LUFactorize()
    {
        factored = new NNMatrix(this);
        operations = new MMSummary(n);
        equations = new MMSummary(n);


        for (int k = 0; k < n; k++)
        {
            for (int i = k + 1; i < n; i++)
            {

                double mult = factored.values[i, k] / factored.values[k, k];
                string sEqnMult = "(" + equations.summary[i, k] + "/" + equations.summary[k, k] + ")";
                string sOpsMult = "(" + factored.values[i, k] + "/" + factored.values[k, k] + ")";

                factored.values[i, k] = mult;
                equations.summary[i, k] = sEqnMult;
                operations.summary[i, k] = sOpsMult;

                for (int j = k + 1; j < n; j++)
                {
                    equations.summary[i, j] = "(" + equations.summary[i, j] + " - " + sEqnMult + "*" + equations.summary[k, j] + ")";
                    operations.summary[i, j] = "(" + factored.values[i, j] + "-" + sOpsMult + "*" + factored.values[k, j] + ")";
                    factored.values[i, j] = factored.values[i, j] - mult * factored.values[k, j];
                }
            }
        }
    }
    public void Transpose()
    { 
        if (values == null) throw new ArgumentNullException("values");
        if (values.GetLength(0) != values.GetLength(1)) throw new ArgumentOutOfRangeException("values", "values is not square");

        int size = values.GetLength(0);

        for (int n = 0; n < (size - 1); ++n)
        {
            for (int m = n + 1; m < size; ++m)
            {
                double temp = values[n, m];
                values[n, m] = values[m, n];
                values[m, n] = temp;
            }
        }
    }

    public NNMatrix ULFactorize()
    {
        NNMatrix UL = new NNMatrix(this);
        UL.operations = new MMSummary(n);
        UL.equations = new MMSummary(n);



        for (int k = 0; k < n; k++)
        {
            for (int i = k + 1; i < n; i++)
            {

                double mult = UL.values[i, k] / UL.values[k, k];
                string sEqnMult = "(" + UL.equations.summary[i, k] + "/" + UL.equations.summary[k, k] + ")";
                string sOpsMult = "(" + UL.values[i, k] + "/" + UL.values[k, k] + ")";

                UL.values[i, k] = mult;
                UL.equations.summary[i, k] = sEqnMult;
                UL.operations.summary[i, k] = sOpsMult;

                for (int j = k + 1; j < n; j++)
                {
                    UL.equations.summary[i, j] = "(" + UL.equations.summary[i, j] + " - " + sEqnMult + "*" + UL.equations.summary[k, j] + ")";
                    UL.operations.summary[i, j] = "(" + UL.values[i, j] + "-" + sOpsMult + "*" + UL.values[k, j] + ")";
                    UL.values[i, j] = UL.values[i, j] - mult * UL.values[k, j];
                }

            }

        }

        return UL;
    }


    public NNMatrix getLower()
    {
        NNMatrix lower = new NNMatrix(this);
        for (int k = 0; k < n; k++)
        {
            lower.values[k, k] = 1;
            for (int i = k + 1; i < n; i++)
            {
                lower.values[k, i] = 0;
            }
        }

        return lower;
    }
    public NNMatrix getUpper()
    {
        NNMatrix lower = new NNMatrix(this);
        for (int k = 0; k < n; k++)
        {
            for (int i = k + 1; i < n; i++)
            {
                lower.values[i, k] = 0;
            }
        }

        return lower;
    }
    public NNMatrix multiply(NNMatrix other)
    {
        NNMatrix result = new NNMatrix(this);
        for (int k = 0; k < n; k++)
        {
            for (int i = 0; i < n; i++)
            {

                result.values[k, i] = 0;
                for (int j = 0; j < n; j++)
                {
                    result.values[k, i] += this.values[k, j] * other.values[j, i];
                }
            }
        }
        result.LUFactorize();
        return result;
    }
    public void print()
    {

        for (int k = 0; k < n; k++)
        {
            if (k == 0)
            {
                Console.Write("[");
            }
            else
            {
                Console.Write(" ");
            }
            for (int i = 0; i < n; i++)
            {
                Console.Write(values[k, i]);
                if (i < n - 1)
                {
                    Console.Write(",");
                }
            }
            if (k == n - 1)
            {
                Console.WriteLine("]");
            }
            else
            {
                Console.WriteLine("");
            }
        }

    }








}
class Program
{
    static void Main()
    {
        NNMatrix B = new NNMatrix(new double[,] { { 1, 2, 4 }, { 3, 8, 14 }, { 2, 6, 13 } }, 3);
        //NNMatrix B = new NNMatrix(new double[,] { { 2, 5, -2 }, { 1, 2, -1 }, { 2, 5, -4 } }, 3);


        Console.WriteLine("A Matrix, upper, Lower");
        B.factored.getLower().print();
        B.factored.getUpper().print();

        Console.WriteLine("AA Matrix, upper, Lower");
        NNMatrix Bcopy = new NNMatrix(B);
        NNMatrix BB = B.multiply(Bcopy);
        BB.print();
        BB.factored.print();

        Console.WriteLine("AA Matrix, upper, Lower");
        NNMatrix BLU = B.factored.getLower().multiply(B.factored.getUpper());
        BLU.multiply(BLU).print();
       
        Console.ReadLine(); //Pause

        
    }
}
