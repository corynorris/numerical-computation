Vx = 65;
Vy = 30;

%Vx=40;
%Vy=30;

disp('Vx, Vy'); disp([Vx; Vy]);

tspan = [0 5];

initial = [0; 0; Vy];

tol = 1.e-7;

options = odeset('Events', @golf_events, 'Refine', 2, 'MaxStep', 1./Vx);

[t,z,tevent,yevent,eventNum] = ode45(@golf,tspan,initial,options,Vx);

N = length(t)

disp('tfinal, Xfinal,Yfinal');
disp([t(N),z(N,1), z(N,2)]);
