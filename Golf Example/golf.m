function values = golf(t, u, horizVel)
% u(1) = x(t) ; u(2) = y(t) ; u(3) = y’(t)
% default parameter list size is extended by
% one extra parameter, horizVel
%horiz = 100;        % ft/s
    values = [horizVel; u(3); -32.2];
