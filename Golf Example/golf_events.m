function [values, isterminal, direction] = golf_events(t, z , Vx)

% values, isterminal, direction are all vectors (in this case of length 2)
%
% values: positive or negative.  An event is a change of sign
%         (indicating a zero was encountered at an intermediate
%         timestep)
% isterminal: 1 if solver is to terminate, 0 if it should keep
%         going when an even it encountered
% direction: 0 if all zeros of values are to be used (use this!)

    
% z(1) = x(t); z(2) = y(t); z(3) = y’(t)
% Vx = horizontal velocity - weird Matlab quirk -
%          the dynamics function and the events functions MUST have
%          same number of extra parameters. We don’t need Vx here
%          but we do in the dynamics function, golfsdf

% barrier has average height = bHeight ;
%     centered on fieldsize
%     and fluctuates by 3 feet once in 2 seconds = i.e. with frequency .5

    f = .5;          % barrier frequency
    fieldSize = 90;  % ft
    bHeight = 5;    % so barrier fluctuates between 9 and 15 feet

    values = [0,0]';
    isterminal = [1,1]';
    direction = [0,0]';

    % Event 1: the ball hits the ground (the ground is located at z(2)=y=0).
    values(1) = z(2); %change of sign (+ to -) indicates event 1
    if (z(2)<0)
        disp('ball hits the ground')
        disp(t)
    end;

    % Event 2: the ball hits the barrier
    bDyn   = bHeight + 3 * cos(2 * pi * f * t);  %dynamic barrier height
    values(2) = 1;
    if (abs(z(1) - fieldSize) < 1) && z(2) < bDyn
        disp('ball hits barrier');
        disp(t)
        values(2) = -1; 
    end;
