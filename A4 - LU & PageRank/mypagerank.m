function [p_next,cnt] = mypagerank(U,G)
% Develop Matlab code for determining the pagerank of a set
% of Web pages given by G and U. You should use the iterative method
% described in the course notes. Inside your iteration loop, you should
% only require a sparse matrix multiply, dot-products, scaler-vector mul-
% tiplies, and vector adds. No other explicit loops should be used (inside
% the iteration loop).


% for testing
% T = [0,0,1/3,0,0,0; 1/2,0,1/3,0,0,0; 1/2,0,0,0,0,0;0,0,0,0,1/2,1;0,0,1/3,1/2,0,0; 0,0,0,1/2,1/2,0];
% T = [0,0,1,0,0,0; 1,0,1,0,0,0; 1,0,0,0,0,0;0,0,0,0,1,1;0,0,1,1,0,0; 0,0,0,1,1,0];

% Initial Parameters
tol = 10^-5;
delta = 0.8;
[n,n] = size(G);
R = n;
e = ones(n,1);
cnt = 0;


% Pre-process G to construct P
% eliminate self-referencial links
P = G; %- diag(diag(G));
c = sum(P,1); %sum of columns
r = sum(P,2); %sum of columns
d = sparse(n,1);

% Create d for sparcity and transform to probability matrix
for i = 1:n
   if c(i) == 0
        d(i) = 1;
   else
       P(:,i) = P(:,i)/c(i); 
   end
end



%P is now equivilant to Q
%M = delta*P + ((1-delta)*e*transpose(e) / R);


% Any column of zeroes should be 1/R
%M = delta*G + ((1-delta)/R)*(e*e.');
%x = M;

% P0 = e/R
p = sparse(n,1);

% Storage for the next p
p_next = sparse(e/R);

while max(abs(p_next-p)) > tol
    
    % Store previous p
    p = p_next;

    % We want.  This is really sparse, but all this multiplying
    % leads to some error.
    p_next = delta*(P*p + e*(transpose(d)*p/R)) + (1-delta)*(e/R);

    
    % keep track of how many iterations
    cnt = cnt+1;
    
end

%display G, P, a bargraph or rank and the first 50 urls
subplot(2,2,1), spy(G); title('G, including self-referential links');
subplot(2,2,2), spy(P); title('G, excluding self-referential links');
subplot(2,1,2), bar(p_next); title('Bargraph of rank');

full_p = full(p_next);
full_r = full(r);
full_c = full(c);
[ignore,q] = sort(-full_p);
disp(' rank org-odr  page-rk  in  out  url')
k = 1;
while (k <= 50)
   j = q(k);
   disp(sprintf(' %4.0f %4.0f %10.6f %4.0f %4.0f  %s', ...
      k, j,full_p(j),full_r(j),full_c(j),U{j}))
   k = k+1;
end
