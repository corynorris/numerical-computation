function values = dynamics(times, xy)
    % St = speed of target
    % Sp = speed of projectile
    St = 1.0;
    Sp = 2.0;
    
    % Rt = minimum turning radius of target
    % Rp = minimum turning radius of projectile
    Rt = 0.25;    
    Rp = 1.0;
        
    % unpack our xy matrix to make it more readable
    Tx = xy(1);
    Ty = xy(2);
    Px = xy(3);
    Py = xy(4);
    Td = xy(5);
    Pd = xy(6);
    
    % Wt = angular speed of target
    % Adjusted to be quicker if the pursuer is close.
    Wt = 0.1;
    proximity = sqrt( (Px - Tx)^2 + (Py-Ty)^2 );
    if (proximity < 1)
        Wt = 1;
    end
    
    % ThetaT = Target Direction
    % note: St, Rt and Wt are all constants
    dThetaT = (St/Rt) * Wt;
    
    % Use the provided function to get teh direction of the projectile 
    % theta_dir = direction( x_t, y_t, x_p, y_p, theta_p)
    angle_to_x_axis = Pd;
    delta_theta_t = direction(Tx, Ty, Px, Py, angle_to_x_axis);
    D = 10 ^-3;
    
    % Sp/Rp * [ delta_theta_t - theta_p  / (|delta_theta_t - theta_p| + D)  
    dThetaP = (Sp/Rp) * ((delta_theta_t - Pd)/(abs(delta_theta_t - Pd)+D));
    
    %ThetaT = cos(t)*xy(1) + sin(t)*xy(2);       
    dyt = St*sin(Td);
    dxt = St*cos(Td);

    dxp = Sp*cos(Pd);
    dyp = Sp*sin(Pd);

    values = [dxt; dyt; dxp; dyp; dThetaT; dThetaP];
    