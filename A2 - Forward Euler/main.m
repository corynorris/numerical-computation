figure

% Options
tol = 10^-7;
tspan = [0 40];

% Target start position: (Tx, Ty) = (5,5).
% Projectile start position: (Px, Py) = (0,0).
% Starting directions respectively are: (Td, Pd) =  (0, -pi)
% Giving yeild to [Tx; Ty; Px; Py; Td; Pd]
initial = [5; 5; 0; 0; 0; -pi];

% Where @events is your events function
options = odeset('AbsTol',tol,'RelTol',tol, 'MaxOrder',5,'Stats','on','Events',@pt_events,'Refine',4);

% Solve the ODE
[Tt,Vt] =ode45(@dynamics, tspan, initial, options);


% Plot the derivatives
subplot(4,1,1), plot(Tt(:,1), Vt(:,5), 'g', Tt(:,1),Vt(:,6),'r');
grid off;

% Plot the projectile pursuinig the target
subplot(4,1,[2 4]), plot(Vt(:,1), Vt(:,2), 'g', Vt(:,3),Vt(:,4),'r');
grid on;
