function [values, isterminal, direction] = pt_events(times, xy)

% values, isterminal, direction are all vectors (in this case of length 2)
%
% values: positive or negative.  An event is a change of sign
%         (indicating a zero was encountered at an intermediate
%         timestep)
% isterminal: 1 if solver is to terminate, 0 if it should keep
%         going when an even it encountered
% direction: 0 if all zeros of values are to be used (use this!)


    % Change these to indicate an event
    isterminal = 0;
    direction = 0;
        
    % Event 1: the projectile hits the target
    values = 1;
    dStop = 10^-2;
    proximity = sqrt( (xy(3) - xy(1))^2 + (xy(4)-xy(2))^2 );


    %disp (proximity);
    if (proximity <= dStop)
       % Display a message and the time of collision 
       % odly, fprintf doesn't show anything for me, so I've left it as
       % such:
        disp(sprintf('Projectile collides with Target after: %s seconds',times));      
       
       %change the value to indicate an event occured
       values = -1;
       isterminal = -1;
    end;
