% Load and plot each portion separately, so they aren't connected by a line
hand = load('hand.mat');
l1 = load('letter_one.mat');
l2 = load('letter_two.mat');
%plot(hand.x,hand.y, l1.x,l1.y,l2.x,l2.y);


% -------------------  HAND  ------------------------
%We need to plot them as piecewise functions over time
len = 43;
Ch = zeros(len,3);
for i = 1:len
    Ch(i,1) = i;
    Ch(i,2) = hand.x(i);
    Ch(i,3) = hand.y(i);
end

% interpolating cubic spline:
% chord length spaced parameters
t = Ch(:,1)';
t(1) = 0;
for i = 2:len
   t(i) = t(i-1)+sqrt((Ch(i,2)-Ch(i-1,2))^2 + (Ch(i,3)-Ch(i-1,3))^2);
end
t;

% Let Cpp be a spline on the parameterized data 
%    (t_i, (x_i,y_i))  for i=1..17
% spline returns a pair of ppforms (Matlab's piecewise polynomial representation)
% one for x(t) and one for y(t)
cpp = csape(t,Ch(:,2:3)','second'); 

%cpp = csape(pp,'second');

% For plotting, sample the spline 5 times as often
% as the number of points we used to build it.
oversampling = 5;
dVec = [0:oversampling-1]/oversampling;
Vec = ones(1,oversampling);
param = zeros(1,(len-1)*oversampling);
for i = 1:len-1
   dt = t(i+1)-t(i);
   param(1+oversampling*(i-1):oversampling*i) = t(i)*Vec+dt*dVec;
end
param((len-1)*oversampling) = t(len);

param;  % display variable

% evluate the piecewise polynomials at the values
% Scalars a, b, and c
a = 1;
b = 1;
c = 1;

% End condition at left
e = 0;

% The cubic spline interpolant s1 to the
% given data using the default end
% condition
s1 = csape(t,Ch(:,2:3)','not-a-knot'); 

% Compute the derivatives of the first
%  polynomial piece of s1 and s0
ds1 = fnder(fnbrk(s1,1));


% Compute interpolant with desired end conditions
%lam1 = a*fnval(ds1,e) + b*fnval(fnder(ds1),e);
%pp = fncmb(s1,lam1);

scriptCh = fnval(s1,param);
fnplt( pp, [594, 632] )
save scriptCh scriptCh



% ------------------- LETTER 1 ------------------------
%We need to plot them as piecewise functions over time
len = 5;
Cl1 = zeros(len,3);
for i = 1:len
    Cl1(i,1) = i;
    Cl1(i,2) = l1.x(i);
    Cl1(i,3) = l1.y(i);
end

% interpolating cubic spline:
% Cl1ord length spaced parameters
t = Cl1(:,1)';
t(1) = 0;
for i = 2:len
   t(i) = t(i-1)+sqrt((Cl1(i,2)-Cl1(i-1,2))^2 + (Cl1(i,3)-Cl1(i-1,3))^2);
end
t;

% Let Cpp be a spline on the parameterized data 
%    (t_i, (x_i,y_i))  for i=1..17
% spline returns a pair of ppforms (Matlab's piecewise polynomial representation)
% one for x(t) and one for y(t)
cpp = csape(t,Cl1(:,2:3)','second'); %SAME AS not-a-knot


%cpp = csape(pp,'second');

% For plotting, sample the spline 5 times as often
% as the number of points we used to build it.
oversampling = 5;
dVec = [0:oversampling-1]/oversampling;
Vec = ones(1,oversampling);
param = zeros(1,(len-1)*oversampling);
for i = 1:len-1
   dt = t(i+1)-t(i);
   param(1+oversampling*(i-1):oversampling*i) = t(i)*Vec+dt*dVec;
end
param((len-1)*oversampling) = t(len);

param;  % display variable

% evluate the piecewise polynomials at the values
scriptCl1 = fnval(cpp,param);

save scriptCl1 scriptCl1


% ------------------- LETTER 2 ------------------------
%We need to plot them as piecewise functions over time
len = 10;
Cl2 = zeros(len,3);
for i = 1:len
    Cl2(i,1) = i;
    Cl2(i,2) = l2.x(i);
    Cl2(i,3) = l2.y(i);
end

% interpolating cubic spline:
% Cl2ord length spaced parameters
t = Cl2(:,1)';
t(1) = 0;
for i = 2:len
   t(i) = t(i-1)+sqrt((Cl2(i,2)-Cl2(i-1,2))^2 + (Cl2(i,3)-Cl2(i-1,3))^2);
end
t;

% Let Cpp be a spline on the parameterized data 
%    (t_i, (x_i,y_i))  for i=1..17
% spline returns a pair of ppforms (Matlab's piecewise polynomial representation)
% one for x(t) and one for y(t)
cpp = csape(t,Cl2(:,2:3)','second'); %SAME AS not-a-knot


%cpp = csape(pp,'second');

% For plotting, sample the spline 5 times as often
% as the number of points we used to build it.
oversampling = 5;
dVec = [0:oversampling-1]/oversampling;
Vec = ones(1,oversampling);
param = zeros(1,(len-1)*oversampling);
for i = 1:len-1
   dt = t(i+1)-t(i);
   param(1+oversampling*(i-1):oversampling*i) = t(i)*Vec+dt*dVec;
end
param((len-1)*oversampling) = t(len);

param;  % display variable

% evluate the piecewise polynomials at the values
scriptCl2 = fnval(cpp,param);

save scriptCl2 scriptCl2
plot(scriptCh(1,:),scriptCh(2,:),scriptCl2(1,:),scriptCl2(2,:),scriptCl1(1,:),scriptCl1(2,:))



% ------------------- PLOT OPTIONS ------------------------
% Make our font better and label our axis
set(0,'DefaultAxesFontSize',15);
xlabel('x') ;
ylabel('y');
title('Initials and Hand, Natural');

% Turn on gridlines
grid off;
axis off;



