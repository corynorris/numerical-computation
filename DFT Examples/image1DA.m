%  Create pattern and signal, or 1-D image 
pat = [0 .3 .6 1.0 .8 .9 .1 0]';

%  Create image that replicates it 4 times
image = zeros(32,1);
image(1:8) = pat;
image(9:16) = pat;
image(17:32) = image(1:16);

%  image(17:24) = ones(8,1);
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultLineLineWidth',2);
figure;

k = 1:32;
plot(k,image,'-s');
axis([1,32,-.5,1.5])

tHandle = title('repetitive pattern - 1D image')

% Compute Fourier coefficients
figure;
colormap(white);
ft = fft(image);
ftMod = sqrt(conj(ft).*ft);
subplot(2,1,1), bar(0:15,ftMod(1:16));
axis([-1,17,-1,22]);
title(' mod of fourier coeff: including DC');
subplot(2,1,2), bar(1:15,ftMod(2:16));
axis([0,17,-1,9]);
title(' mod of fourier coeff: excluding DC');
result = input('<return> to continue.')

% Modify to remove repeated pattern
figure
image(17:24) = ones(8,1);
plot(k,image,'-s');
axis([1,32,-.5,1.5])
tHandle = title('non-repetitive pattern - 1D image')

% Compute newFourier coefficients
figure;
colormap(white);
ft = fft(image);
ftMod = sqrt(conj(ft).*ft);
subplot(2,1,1), bar(0:15,ftMod(1:16));
axis([-1,17,-1,22]);
title(' mod of fourier coeff: including DC');
subplot(2,1,2), bar(1:15,ftMod(2:16));
axis([0,17,-1,9]);
title(' mod of fourier coeff: excluding DC');
result = input('<return> to continue.')

% Compress  non-repetitive pattern
figure
plot(image,'-s')
axis([1,32,-.5,1.5])
hold on;
  ftcomp = ft;
  count = 0;
for i = 1:32
   if(ftMod(i)<1)
      ftcomp(i) = 0;
      count = count+1;
   end
end
disp(' number dropped '); disp(count);
newimage = real(ifft(ftcomp));
plot(newimage,'-rs')
legend('original','compressed')
%plHandle =plot(newimage,'-')
%set(plHandle,'LineWidth',2)
title('Original and compressed 1-D image data')
hold off
