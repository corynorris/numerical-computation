% The recursive FFT function

function F = myFFT(f,U,N)
    if N == 1
        F=f;
    else
        F=[1:N];
        g=[1:N/2];
        h=[1:N/2];
        for n = 1:N/2
            g(n)=0.5*(f(n)+f(n+N/2));
            h(n)=0.5*(f(n)-f(n+N/2))*U^(n-1);
        end;
        Feven = myFFT(g,U^2,N/2);
        Fodd=myFFT(h,U^2,N/2);	
        for n = 0:(N/2-1)
            F(2*n+1) = Feven(n+1);
            F(2*n+2) = Fodd(n+1);
        end
    end
end

       
  