% The basic DFT function, following the definition

function F = DFT(f,U,N)
   for k = 0:N-1
       F(k+1)=0;
       for n = 0:N-1
           F(k+1)=F(k+1)+f(n+1)*U^(n*k);
       end
   end
   F=1/N*F;
end
