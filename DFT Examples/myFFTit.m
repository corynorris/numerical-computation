% Iterative (Butterfly) FFT function
% In-place, but produces DFT in bit-reversed order

function  [f] = myFFTit(f,U,N)
  m = log2(N);
  for k = 1:m
    N=2^(m-k+1);
    for j = 1:2^(k - 1)
      q = (j - 1) * N;
      for n = 0 : N/2-1
        temp = 0.5* (f(n+q+1) - f(n+q+N/2+1)) * U^(n*(2^(k-1)));
        f(n+q+1) = 0.5 * (f(n+q+1) + f(n+q+N/2+1));
        f(n+q+N/2+1) = temp;
      end
    end
  end
  f=bitrevorder(f);
end
