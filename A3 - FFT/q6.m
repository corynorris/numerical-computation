load('inputsound.mat');
soundsc(y,Fs);

% swithc y to x so it reads with the appendix
x = y;

% Plot the sound signal and its FFT
X = fft(x);

subplot(6,1,1), plot(x);
title('Sound Values');
subplot(6,1,2),stem(X);
title('FFT (Frequency)');



% Select p = N/4 to take half the actual frequencies as high's and half as lows
% with the hope that'll be a well selected value.
N = length(X);
p = N/4;


% ------------------------------------------
% Apply Low-Pass Filter
% ------------------------------------------
Q = X; 

% Q = X so no multiplication will be required, we can simply 0 filtered
% values.  Eg, directly apply the filter.
for k=p+1:N-p-1
    Q(k) = 0;
end

% Inverse FFT to get the (now filtered) sound values back
xhat = real(ifft(Q));

% Plot Q and xhat out of curiosity
subplot(6,1,3), stem(Q);
title('Filtered Low-Pass FFT Values');
subplot(6,1,4), plot(xhat);
title('Filtered Low-Pass Sound Values');

%Play the new sound values
soundsc(xhat,Fs);

% ------------------------------------------
% Repeat with High-Pass Filter
% ------------------------------------------
Q = X; 

% Q = X so no multiplication will be required, we can simply 0 filtered
% values.  Eg, directly apply the filter.
for k=N-p-1:N
    Q(k) = 0;
end
for k=1:p+1
    Q(k) = 0;
end

% Inverse FFT to get the (now filtered) sound values back
xhat = real(ifft(Q));

% Plot Q and xhat out of curiosity
subplot(6,1,5), stem(Q);
title('Filtered High-Pass FFT Values');
subplot(6,1,6), plot(xhat);
title('Filtered High-Pass Sound Values');


%Play the new sound values
soundsc(xhat,Fs);