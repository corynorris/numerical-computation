% Load and plot signals 1 and 2
figure;
load ('signal1.txt');
load ('signal3.txt');
load ('signal4.txt');
load ('signal5.txt');
load ('signal6.txt');

% Plot original noisy signals
subplot (4,2,1), plot (signal3);
title ('Signal 3');

subplot (4,2,2), plot (signal4);
title ('Signal 4');

subplot (4,2,3), plot (signal5);
title ('Signal 5');

subplot (4,2,4), plot (signal6);
title ('Signal 6');

% Correlation in each case
corr2 = xcorr(signal1, signal2);
corr3 = xcorr(signal1, signal3);
corr4 = xcorr(signal1, signal4);
corr5 = xcorr(signal1, signal5);
corr6 = xcorr(signal1, signal6);

subplot (4,2,5), plot (corr3);
title ('Correlation 3');

subplot (4,2,6), plot (corr4);
title ('Correlation 4');

subplot (4,2,7), plot (corr5);
title ('Correlation 5');

subplot (4,2,8), plot (corr6);
title ('Correlation 6');


% Estimate
[c1,i1] = max(signal1);
[c2,i2] = max(corr2);
[c3,i3] = max(corr3);
[c4,i4] = max(corr4);
[c5,i5] = max(corr5);
[c6,i6] = max(corr6);

Estimate2 = 2048 - i2; %100
Estimate3 = 2048 - i3; %576
Estimate4 = 2048 - i4; %302
Estimate5 = 2048 - i5; %620
Estimate6 = 2048 - i6; %171

