% Load and plot signals 1 and 2
figure;
load ('signal1.txt');
load ('signal2.txt');

subplot (4,1,1), plot (signal1);
title ('Signal 1');

subplot (4,1,2), plot (signal2);
title ('Signal 2');


% Calculate the correlation function
% take the fft and multiply by its complex conjugate and then inverse fft the result
% Same as r = xcorr(signal1, signal2);
% http://stackoverflow.com/questions/3949324/calculate-autocorrelation-using-fft-in-matlab
r = ifft( fft(signal2) .* conj(fft(signal1)) );
subplot (4,1,[3 4]), plot (r);
title ('Correlation Between Signal 1 and Signal 2');


% Aside, since the correlation shows the shift, we can show that given the
% following:
[c1,i1] = max(signal1);
[c2,i2] = max(signal2);
[c3,i3] = max(r);
% then shifting i1 right by the amount in i3 should yeild i2
% i1+i3 = i2
% i1 = 135
% i2 = 235
% i3 = 101
% or 135 + 101 = 236, off by 1 because of the indexing from 0.
% Therefore, i3 represents distance between i1 and i2, or the amount i1
% must be shifted to the right to line up with i2.
